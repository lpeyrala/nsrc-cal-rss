OVERVIEW:
This program generates RSS feeds for the NSRC activities calendar listed at nsrc.org/calendar.
One RSS feed is for upcoming events, while the other contains all events past, present and future.


BUILD REQUIREMENTS:
The program is written in Go and depends on two packages not bundled with standard Go distribution:
  1. github.com/gorilla/feeds (install with `go get github.com/gorilla/feeds`)
  2. github.com/go-sql-driver/mysql (install with `go get github.com/go-sql-driver/mysql`)

Assuming those packages are installed, the program has been built successfully using Go 1.12.1+
by typing `go build` within this directory. A binary will be produced called `nsrc-cal-rss`


DATABASE CONNECTION DETAILS:
Database connection details can be specified either by setting the following environment variables -OR-
by using the -db-file command line option and making it point to a JSON config file specifying the same variables.
This program reads the database connection details from the environment by default if the -db-file cli option is not specified.

If reading from the environment, these variables must be set:
  CAL_DB_USER=<username>
  CAL_DB_PASS=<password>
  CAL_DB_HOST=<hostname>
  CAL_DB_PORT=<port number>
  CAL_DB_NAME=<database name>

If reading from a JSON config file with the -db-file cli option, the file must have this format:
{
  "CAL_DB_USER": "<username>",
  "CAL_DB_PASS": "<password>",
  "CAL_DB_HOST": "<hostname>",
  "CAL_DB_PORT": "<port number>",
  "CAL_DB_NAME": "<database name>"
}


USAGE:
./nsrc-cal-rss [-h] [-upcoming-events-path=<path>] [-all-events-path=<path>] [-db-file=<path>]
  -upcoming-events-path, path to save the upcoming events RSS feed (efault path is ./upcoming-events.rss)
  -all-events-path, path to save all-events RSS feed (default path is ./all-events.rss)
  -db-file, path to JSON config file specifying database connection details; if not specified, read from environment variables
